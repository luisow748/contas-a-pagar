package com.luisow748.contas_a_pagar.fixture;

import com.luisow748.contas_a_pagar.domain.model.Conta;

import java.math.BigDecimal;

public class ContaFixture {
    public static final String SITUACAO = "PENDENTE";
    public static final String DESCRICAO = "Descrição";

    public static Conta get(){
        return new Conta(
                1L,
                "Conta de Luz",
                "Conta de luz referente ao mês de janeiro",
                BigDecimal.valueOf(150.00),
                "10-07-2024",
                null,
                SITUACAO
        );
    }
}
