package com.luisow748.contas_a_pagar.interfaces.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.luisow748.contas_a_pagar.application.dto.ContaDTO;
import com.luisow748.contas_a_pagar.application.service.AtualizarContaService;
import com.luisow748.contas_a_pagar.application.service.CriacaoContaService;
import com.luisow748.contas_a_pagar.application.service.ListarContaService;
import com.luisow748.contas_a_pagar.application.service.ValorContaService;
import com.luisow748.contas_a_pagar.interfaces.api.controller.ContaController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ContaController.class)
@AutoConfigureMockMvc(addFilters = false)
public class ContaControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CriacaoContaService criacaoContaService;

    @MockBean
    private AtualizarContaService atualizarContaService;

    @MockBean
    private ListarContaService listarContaService;

    @MockBean
    private ValorContaService valorContaService;

    @Autowired
    private ObjectMapper objectMapper;

    private ContaDTO contaDTO;

    @BeforeEach
    public void setUp() {
        contaDTO = new ContaDTO(
                "Conta",
                "Descrição válida",
                String.valueOf(BigDecimal.TEN),
                "10-07-2024",
                null
        );
    }

    @Test
    public void testCriarContaSemNome() throws Exception {
        contaDTO.setNome(null);

        mockMvc.perform(post("/api/contas")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(contaDTO)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.nome").value("Nome da conta não pode ser nulo"));
    }
    @Test
    public void testCriarContaValida() throws Exception {

        mockMvc.perform(post("/api/contas")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(contaDTO)))
                .andExpect(status().isOk());
    }
}
