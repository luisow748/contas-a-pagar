package com.luisow748.contas_a_pagar.application.service;

import com.luisow748.contas_a_pagar.application.dto.ContaDTO;
import com.luisow748.contas_a_pagar.domain.model.Conta;
import com.luisow748.contas_a_pagar.domain.model.events.ContaAtualizadaEvent;
import com.luisow748.contas_a_pagar.fixture.ContaFixture;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class AtualizarContaServiceTest {

    private ContaService contaService;
    private AtualizarContaService atualizarContaService;

    @BeforeEach
    public void setUp() {
        contaService = mock(ContaService.class);
        atualizarContaService = new AtualizarContaService(contaService);
        when(contaService.findById(any())).thenReturn(ContaFixture.get());
    }

    @Test
    public void deveAtualizarConta() {
        String novaDescricao = "Nova descrição";
        String novaSituacao = "PAGO";
        ContaDTO contaDTO = new ContaDTO("Nome", novaDescricao, "150.00", "10-07-2024", novaSituacao);
        atualizarContaService.atualizarConta(1L, contaDTO);

        ArgumentCaptor<Conta> contaCaptor = ArgumentCaptor.forClass(Conta.class);
        ArgumentCaptor<ContaAtualizadaEvent> eventCaptor = ArgumentCaptor.forClass(ContaAtualizadaEvent.class);
        verify(contaService, times(1)).save(contaCaptor.capture(), eventCaptor.capture());

        Conta savedConta = contaCaptor.getValue();
        assertEquals(novaDescricao, savedConta.getDescricao());
        assertEquals(novaSituacao, savedConta.getSituacao());
    }
}