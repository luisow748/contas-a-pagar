package com.luisow748.contas_a_pagar.application.service;

import com.luisow748.contas_a_pagar.application.dto.ContaDTO;
import com.luisow748.contas_a_pagar.domain.model.Conta;
import com.luisow748.contas_a_pagar.domain.model.events.ContaCriadaEvent;
import com.luisow748.contas_a_pagar.fixture.ContaFixture;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class CriacaoContaServiceTest {

    private ContaService contaService;
    private CriacaoContaService criacaoContaService;

    @BeforeEach
    public void setUp() {
        contaService = mock(ContaService.class);
        criacaoContaService = new CriacaoContaService(contaService);
    }

    @Test
    public void deveCriarContaComValoresCorretos() {
        ContaDTO contaDTO = new ContaDTO("Nome", ContaFixture.DESCRICAO, "150.00", "10-07-2024", ContaFixture.SITUACAO);
        criacaoContaService.criarConta(contaDTO);

        ArgumentCaptor<Conta> contaCaptor = ArgumentCaptor.forClass(Conta.class);
        ArgumentCaptor<ContaCriadaEvent> eventCaptor = ArgumentCaptor.forClass(ContaCriadaEvent.class);
        verify(contaService, times(1)).save(contaCaptor.capture(), eventCaptor.capture());

        Conta savedConta = contaCaptor.getValue();
        assertEquals(ContaFixture.DESCRICAO, savedConta.getDescricao());
        assertEquals(ContaFixture.SITUACAO, savedConta.getSituacao());
    }

    @Test
    public void naoDevePermitirCriacaoDeContaComValorIncorreto() {
        ContaDTO contaDTO = new ContaDTO("Nome", ContaFixture.DESCRICAO, "150,00", "10-07-2024", ContaFixture.SITUACAO);
        assertThrows(NumberFormatException.class, () -> criacaoContaService.criarConta(contaDTO));
        verify(contaService, never()).save(any());
    }
}
