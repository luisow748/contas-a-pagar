CREATE TABLE IF NOT EXISTS conta (
                              id int8 NOT NULL,
                              data_pagamento date NULL,
                              data_vencimento date NOT NULL,
                              descricao varchar(100) NULL,
                              nome varchar(50) NOT NULL,
                              situacao varchar(8) NULL,
                              valor numeric(38, 2) NOT NULL,
                              CONSTRAINT conta_pkey PRIMARY KEY (id)
);