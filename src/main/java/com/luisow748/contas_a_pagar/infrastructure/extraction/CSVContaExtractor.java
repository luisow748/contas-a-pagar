package com.luisow748.contas_a_pagar.infrastructure.extraction;

import com.luisow748.contas_a_pagar.application.dto.ContaDTO;
import com.luisow748.contas_a_pagar.application.utils.CSVBeanUtils;
import com.opencsv.bean.CsvToBean;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

@Component
public class CSVContaExtractor implements ContaExtractor {

    @Override
    public List<ContaDTO> extrairContas(InputStream inputStream) {
        try (InputStreamReader reader = new InputStreamReader(inputStream)) {
            CsvToBean<ContaDTO> csvToBean = CSVBeanUtils.from(reader, ContaDTO.class);
            return csvToBean.stream().toList();
        } catch (Exception e) {
            throw new RuntimeException("Erro ao importar contas do CSV", e);
        }
    }
}
