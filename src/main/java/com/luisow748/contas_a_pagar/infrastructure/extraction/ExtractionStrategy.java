package com.luisow748.contas_a_pagar.infrastructure.extraction;

import com.luisow748.contas_a_pagar.application.dto.ContaDTO;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ExtractionStrategy {

    private final Map<String, ContaExtractor> extractors = new HashMap<>();

    public ExtractionStrategy(List<ContaExtractor> extractorList) {
        for (ContaExtractor extractor : extractorList) {
            if (extractor instanceof CSVContaExtractor) {
                extractors.put("csv", extractor);
            }
        }
    }

    public List<ContaDTO> extract(String format, InputStream inputStream) {
        return extractors.get(format).extrairContas(inputStream);
    }
}
