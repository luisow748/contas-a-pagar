package com.luisow748.contas_a_pagar.infrastructure.extraction;


import com.luisow748.contas_a_pagar.application.dto.ContaDTO;

import java.io.InputStream;
import java.util.List;

public interface ContaExtractor {
    List<ContaDTO> extrairContas(InputStream inputStream);
}