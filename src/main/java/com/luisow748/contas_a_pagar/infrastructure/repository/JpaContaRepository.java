package com.luisow748.contas_a_pagar.infrastructure.repository;

import com.luisow748.contas_a_pagar.infrastructure.entity.ContaEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface JpaContaRepository extends JpaRepository<ContaEntity, Long> {

    Page<ContaEntity> findByDescricao(Pageable pageable, String descricao);

    Page<ContaEntity> findByDataVencimento(Pageable pageable, LocalDate dataVencimento);
    Page<ContaEntity> findAll(Pageable pageable);

    @Query("SELECT SUM(c.valor) FROM conta c WHERE c.dataPagamento BETWEEN :dataPagamentoInicial AND :dataPagamentoFinal and c.situacao = 'PAGO'")
    Double sumByDataPagamentoBetweenPago(@Param("dataPagamentoInicial") LocalDate dataPagamentoInicial, @Param("dataPagamentoFinal") LocalDate dataPagamentoFinal);
}