package com.luisow748.contas_a_pagar.infrastructure.repository;

import com.luisow748.contas_a_pagar.application.utils.DateUtils;
import com.luisow748.contas_a_pagar.domain.model.Conta;
import com.luisow748.contas_a_pagar.domain.repository.ContaRepository;
import com.luisow748.contas_a_pagar.infrastructure.entity.ContaEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class ContaRepositoryImpl implements ContaRepository {

    private final JpaContaRepository jpaContaRepository;

    @Override
    public void save(Conta conta) {
        jpaContaRepository.save(mapToEntity(conta));
    }

    @Override
    public void saveAll(List<Conta> contas) {
        jpaContaRepository.saveAll(contas.stream().map(this::mapToEntity).toList());
    }

    @Override
    public Page<Conta> findByDataVencimento(Pageable pageable, String dataVencimento) {
        return jpaContaRepository.findByDataVencimento(pageable, DateUtils.getDate(dataVencimento)).map(this::mapToDomain);

    }

    @Override
    public Page<Conta> findByDescricao(Pageable pageable, String descricao) {
        return jpaContaRepository.findByDescricao(pageable, descricao).map(this::mapToDomain);
    }

    @Override
    public Optional<Conta> findById(Long id) {
        return jpaContaRepository.findById(id).map(this::mapToDomain);
    }

    @Override
    public BigDecimal getSomaPagoEntreDatas(String dataPagamentoInicial, String dataPagamentoFinal) {
        return BigDecimal.valueOf(jpaContaRepository.sumByDataPagamentoBetweenPago(DateUtils.getDate(dataPagamentoInicial), DateUtils.getDate(dataPagamentoFinal)));
    }

    @Override
    public Page<Conta> findAll(Pageable pageable) {
        return jpaContaRepository.findAll(pageable).map(this::mapToDomain);
    }

    private Conta mapToDomain(ContaEntity contaEntity) {
        return new Conta(
                contaEntity.getId(),
                contaEntity.getNome(),
                contaEntity.getDescricao(),
                contaEntity.getValor(),
                DateUtils.getValue(contaEntity.getDataVencimento()),
                DateUtils.getValue(contaEntity.getDataPagamento()),
                contaEntity.getSituacao());
    }

    private ContaEntity mapToEntity(Conta conta) {
        ContaEntity contaEntity = new ContaEntity();
        contaEntity.setId(conta.getId());
        contaEntity.setNome(conta.getNome());
        contaEntity.setDescricao(conta.getDescricao());
        contaEntity.setValor(conta.getValor());
        contaEntity.setDataVencimento(DateUtils.getDate(conta.getDataVencimento()));
        contaEntity.setSituacao(conta.getSituacao());
        if(Objects.nonNull(conta.getDataPagamento()) && !conta.getDataPagamento().isBlank()){
            contaEntity.setDataPagamento(DateUtils.getDate(conta.getDataPagamento()));
        }
        return contaEntity;
    }
}
