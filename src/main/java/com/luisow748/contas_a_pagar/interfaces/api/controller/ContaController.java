package com.luisow748.contas_a_pagar.interfaces.api.controller;

import com.luisow748.contas_a_pagar.application.dto.ContaDTO;
import com.luisow748.contas_a_pagar.application.service.*;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;

@RestController
@RequestMapping("/api/contas")
@RequiredArgsConstructor
public class ContaController {
    private final CriacaoContaService criacaoContaService;
    private final AtualizarContaService atualizarContaService;
    private final ListarContaService listarContaService;
    private final ValorContaService valorContaService;
    private final ImportarContaService contaService;

    @PostMapping
    @Operation(summary = "Criação de Conta")
    public ResponseEntity<ContaDTO> criarConta(@Valid @RequestBody ContaDTO contaDTO) {
        return ResponseEntity.ok(criacaoContaService.criarConta(contaDTO));
    }

    @PutMapping("/{id}")
    @Operation(summary = "Atualização de Conta")
            public ResponseEntity<ContaDTO> atualizarConta(@PathVariable Long id, @Valid @RequestBody ContaDTO contaDTO) {
        return ResponseEntity.ok(atualizarContaService.atualizarConta(id, contaDTO));
    }

    @PutMapping("/{id}/pagar")
    @Operation(summary = "Pagar Conta")
    public ResponseEntity<ContaDTO> pagarConta(@PathVariable Long id) {
        return ResponseEntity.ok(atualizarContaService.pagarConta(id));
    }

    @GetMapping
    @Operation(summary = "Listar Contas")
            public ResponseEntity<Page<ContaDTO>> listarContas(
            @RequestParam(required = false, defaultValue = "0") Integer page,
            @RequestParam(required = false, defaultValue = "10") Integer size,
            @RequestParam(required = false) String dataVencimento,
            @RequestParam(required = false) String descricao
    ) {
        return ResponseEntity.ok(listarContaService.porDataOuDescricao(PageRequest.of(page,size), dataVencimento, descricao));
    }

    @GetMapping("/{id}")
    @Operation(summary = "Buscar Conta por Id")
            public ResponseEntity<ContaDTO> buscarConta(@PathVariable Long id) {
        return ResponseEntity.ok(listarContaService.porId(id));
    }

    @GetMapping("/total-pago/periodo")
    @Operation(summary = "Somar Total Pago por Período")
            public ResponseEntity<BigDecimal> somarTotalPagoPorPeriodo(
            @RequestParam(required = false) String dataInicial,
            @RequestParam(required = false) String dataFinal
    ) {
        return ResponseEntity.ok(valorContaService.getSomaValorPagoPorPeriodo(dataInicial, dataFinal));
    }

    @PostMapping(value = "/importar", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "Importar Contas", description = "Importar contas a pagar. Formato atualmente suportado: .csv")
    public ResponseEntity<Void> importarContas(@RequestParam("tipo") String tipo, @RequestPart(value = "file", required = false) MultipartFile file) {
        try {
            contaService.importarContas(tipo, file.getInputStream());
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
