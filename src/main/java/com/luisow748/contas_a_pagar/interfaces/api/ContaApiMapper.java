package com.luisow748.contas_a_pagar.interfaces.api;

import com.luisow748.contas_a_pagar.application.dto.ContaDTO;
import com.luisow748.contas_a_pagar.domain.model.Conta;
import com.luisow748.contas_a_pagar.domain.model.events.ContaAtualizadaEvent;
import com.luisow748.contas_a_pagar.domain.model.events.ContaCriadaEvent;

import java.math.BigDecimal;
import java.util.Objects;

public class ContaApiMapper {

    public static ContaDTO toContaDTO(Conta conta) {
        var contaDto = new ContaDTO(
                conta.getNome(),
                conta.getDescricao(),
                String.valueOf(conta.getValor()),
                conta.getDataVencimento(),
                conta.getSituacao()
        );
        if (Objects.nonNull(conta.getDataPagamento())) {
            contaDto.setDataPagamento(conta.getDataPagamento());
        }
        return contaDto;
    }

    public static Conta toConta(ContaDTO contaDTO) {
        var conta = new Conta(
                contaDTO.getNome(),
                contaDTO.getDescricao(),
                new BigDecimal(contaDTO.getValor()),
                contaDTO.getDataVencimento(),
                contaDTO.getSituacao()
        );
        if (Objects.nonNull(contaDTO.getDataPagamento())) {
            conta.setDataPagamento(contaDTO.getDataPagamento());
        }
        return conta;
    }

    public static ContaCriadaEvent toCriadaEvent(Conta conta) {
        return new ContaCriadaEvent(
                conta.getNome(),
                conta.getDescricao(),
                conta.getValor(),
                conta.getDataVencimento(),
                conta.getSituacao()
        );
    }

    public static ContaAtualizadaEvent toAtualizadaEvent(Conta conta) {
        return new ContaAtualizadaEvent(
                conta.getNome(),
                conta.getDescricao(),
                conta.getValor(),
                conta.getDataVencimento(),
                conta.getSituacao()
        );
    }
}
