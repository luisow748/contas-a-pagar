package com.luisow748.contas_a_pagar;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info=@Info(title="API Contas a Pagar"))
public class ContasAPagarApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContasAPagarApplication.class, args);
	}

}
