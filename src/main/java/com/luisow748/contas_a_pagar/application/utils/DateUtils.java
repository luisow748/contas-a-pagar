package com.luisow748.contas_a_pagar.application.utils;

import java.time.LocalDate;

public class DateUtils {

    public static String getValue(LocalDate date) {
        if(date == null) {
            return null;
        }
        return date.format(java.time.format.DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    }

    public static LocalDate getDate(String date) {
        if(date == null) {
            return null;
        }
        return LocalDate.parse(date, java.time.format.DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    }
}
