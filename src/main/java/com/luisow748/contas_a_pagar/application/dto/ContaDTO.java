package com.luisow748.contas_a_pagar.application.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Check;

import java.util.Objects;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class ContaDTO {

    @NotNull(message = "Nome da conta não pode ser nulo")
    @Size(min = 3, max = 50, message = "Nome deve ter entre 3 e 50 caracteres")
    private String nome;

    @Size(max = 100, message = "Descrição da conta deve ter no máximo 100 caracteres")
    private String descricao;

    @NotNull(message = "Valor da conta não pode ser nulo")
    @Pattern(regexp = "\\d+(\\.\\d{2})?", message = "Valor deve ser um número decimal")
    private String valor;

    @NotNull(message = "Data de vencimento da conta não pode ser nula")
    @Pattern(regexp = "\\d{2}-\\d{2}-\\d{4}", message = "Data de vencimento da conta deve estar no formato dd-MM-yyyy")
    @JsonProperty("data_vencimento")
    private String dataVencimento;

    @Pattern(regexp = "\\d{2}-\\d{2}-\\d{4}", message = "Data de vencimento da conta deve estar no formato dd-MM-yyyy")
    @JsonProperty("data_pagamento")
    private String dataPagamento;

    @Check(constraints = "situacao IN ('PENDENTE', 'PAGO')")
    private String situacao;

    public ContaDTO(String nome, String descricao, String valor, String dataVencimento, String situacao) {
        this.nome = nome;
        this.descricao = descricao;
        this.valor = valor;
        this.dataVencimento = dataVencimento;
        this.situacao = situacao;
    }

    public String getValor() {
        return Objects.nonNull(valor) ? valor : "0";
    }
}