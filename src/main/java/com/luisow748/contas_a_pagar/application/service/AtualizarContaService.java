package com.luisow748.contas_a_pagar.application.service;

import com.luisow748.contas_a_pagar.application.dto.ContaDTO;
import com.luisow748.contas_a_pagar.domain.model.Conta;
import com.luisow748.contas_a_pagar.interfaces.api.ContaApiMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class AtualizarContaService {
    private final ContaService contaService;

    public ContaDTO atualizarConta(Long id, ContaDTO contaDTO) {
        Conta conta = contaService.findById(id);
        conta.setDescricao(contaDTO.getDescricao());
        conta.setNome(contaDTO.getNome());
        conta.setSituacao(contaDTO.getSituacao());
        if(Objects.nonNull(contaDTO.getDataPagamento())){
            conta.setDataPagamento(contaDTO.getDataPagamento());
        }
        contaService.save(conta, ContaApiMapper.toAtualizadaEvent(conta));
        return contaDTO;
    }

    public ContaDTO pagarConta(Long id) {
        Conta conta = contaService.findById(id);
        conta.pagar();
        contaService.save(conta, ContaApiMapper.toAtualizadaEvent(conta));
        return ContaApiMapper.toContaDTO(conta);
    }
}
