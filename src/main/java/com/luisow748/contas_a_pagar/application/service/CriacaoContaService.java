package com.luisow748.contas_a_pagar.application.service;

import com.luisow748.contas_a_pagar.application.dto.ContaDTO;
import com.luisow748.contas_a_pagar.domain.model.Conta;
import com.luisow748.contas_a_pagar.interfaces.api.ContaApiMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CriacaoContaService {
    private final ContaService contaService;

    public ContaDTO criarConta(ContaDTO contaDTO) {
        contaDTO.setSituacao("PENDENTE");
        Conta conta = ContaApiMapper.toConta(contaDTO);
        contaService.save(conta, ContaApiMapper.toCriadaEvent(conta));
        return contaDTO;
    }
}
