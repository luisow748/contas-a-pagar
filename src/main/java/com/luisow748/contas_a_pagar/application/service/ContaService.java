package com.luisow748.contas_a_pagar.application.service;

import com.luisow748.contas_a_pagar.application.exceptions.ContaNotFoundException;
import com.luisow748.contas_a_pagar.domain.model.Conta;
import com.luisow748.contas_a_pagar.domain.repository.ContaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ContaService {
    private final ContaRepository contaRepository;
    private final ApplicationEventPublisher eventPublisher;

    public Conta findById(Long id) {
        return contaRepository.findById(id)
                .orElseThrow(() -> new ContaNotFoundException(String.valueOf(id)));
    }

    public void save(Conta conta) {
        contaRepository.save(conta);
    }

    public void save(Conta conta, Object event){
        save(conta);
        eventPublisher.publishEvent(event);
    }

    public void saveAll(List<Conta> contas, Object event){
        contaRepository.saveAll(contas);
        eventPublisher.publishEvent(event);
    }

    public Page<Conta> findByDataVencimento(Pageable pageable, String dataVencimento) {
        return contaRepository.findByDataVencimento(pageable, dataVencimento);
    }

    public Page<Conta> findByDescricao(Pageable pageable, String descricao) {
        return contaRepository.findByDescricao(pageable, descricao);
    }

    public Page<Conta> findAll(Pageable pageable) {
        return contaRepository.findAll(pageable);
    }

    public BigDecimal sumByDataPagamentoBetween(String dataVencimentoInicial, String dataVencimentoFinal) {
        return contaRepository.getSomaPagoEntreDatas(dataVencimentoInicial, dataVencimentoFinal);
    }
}
