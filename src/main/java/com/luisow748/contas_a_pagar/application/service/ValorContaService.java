package com.luisow748.contas_a_pagar.application.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
public class ValorContaService {
    private final ContaService contaService;

    public BigDecimal getSomaValorPagoPorPeriodo(String dataPagamentoInicial, String dataPagamentoFinal) {
        return contaService.sumByDataPagamentoBetween(dataPagamentoInicial, dataPagamentoFinal);
    }
}
