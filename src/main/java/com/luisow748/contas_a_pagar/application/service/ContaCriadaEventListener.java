package com.luisow748.contas_a_pagar.application.service;

import com.luisow748.contas_a_pagar.domain.model.events.ContaCriadaEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class ContaCriadaEventListener {

    @EventListener
    public void onContaCriada(ContaCriadaEvent event) {
        System.out.println("Conta criada: " + event.nome() + " - " + event.valor());
    }
}
