package com.luisow748.contas_a_pagar.application.service;

import com.luisow748.contas_a_pagar.domain.model.Conta;
import com.luisow748.contas_a_pagar.domain.model.events.ImportacaoContasEvent;
import com.luisow748.contas_a_pagar.infrastructure.extraction.ExtractionStrategy;
import com.luisow748.contas_a_pagar.interfaces.api.ContaApiMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ImportarContaService {
    private final ContaService contaService;
    private final ExtractionStrategy extractionStrategy;

    public void importarContas(String format, InputStream inputStream) {
        List<Conta> contas = extractionStrategy.extract(format, inputStream).stream().map(ContaApiMapper::toConta).toList();
        contaService.saveAll(contas, new ImportacaoContasEvent(contas));
    }
}
