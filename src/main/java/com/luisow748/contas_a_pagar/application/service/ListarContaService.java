package com.luisow748.contas_a_pagar.application.service;

import com.luisow748.contas_a_pagar.application.dto.ContaDTO;
import com.luisow748.contas_a_pagar.interfaces.api.ContaApiMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ListarContaService {
    private final ContaService contaService;

    public Page<ContaDTO> porDataOuDescricao(PageRequest pageRequest, String dataVencimento, String descricao) {
        if (dataVencimento != null) {
            return contaService.findByDataVencimento(pageRequest, dataVencimento)
                    .map(ContaApiMapper::toContaDTO);
        }
        if (descricao != null) {
            return contaService.findByDescricao(pageRequest, descricao)
                    .map(ContaApiMapper::toContaDTO);
        }
        return contaService.findAll(pageRequest).map(ContaApiMapper::toContaDTO);
    }

    public ContaDTO porId(Long id) {
        return ContaApiMapper.toContaDTO(contaService.findById(id));
    }
}
