package com.luisow748.contas_a_pagar.application.exceptions;

public class ContaNotFoundException extends RuntimeException{
    public ContaNotFoundException(String id) {
        super("Conta " + id + " não encontrada");
    }
    public ContaNotFoundException() {
        super("Nenhuma conta encontrada");
    }
}
