package com.luisow748.contas_a_pagar.domain.repository;

import com.luisow748.contas_a_pagar.domain.model.Conta;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface ContaRepository {
    void save(Conta conta);
    void saveAll(List<Conta> contas);
    Page<Conta> findByDataVencimento(Pageable pageable, String dataVencimento);
    Page<Conta> findByDescricao(Pageable pageable, String descricao);
    Optional<Conta> findById(Long id);
    BigDecimal getSomaPagoEntreDatas(String dataVencimentoInicial, String dataVencimentoFinal);
    Page<Conta> findAll(Pageable pageable);
}
