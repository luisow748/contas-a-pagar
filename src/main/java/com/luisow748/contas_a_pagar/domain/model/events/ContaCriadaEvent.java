package com.luisow748.contas_a_pagar.domain.model.events;

import java.math.BigDecimal;

public record ContaCriadaEvent(String nome, String descricao, BigDecimal valor, String dataVencimento, String situacao) {

    @Override
    public String toString() {
        return "ContaCriadaEvent{" +
                "nome='" + nome + '\'' +
                ", descricao='" + descricao + '\'' +
                ", valor=" + valor +
                ", dataVencimento='" + dataVencimento + '\'' +
                ", situacao='" + situacao + '\'' +
                '}';
    }
}
