package com.luisow748.contas_a_pagar.domain.model.events;

import com.luisow748.contas_a_pagar.domain.model.Conta;

import java.util.List;

public record ImportacaoContasEvent(List<Conta> contas) {

    @Override
    public String toString() {
        return "ImportacaoContasEvent{" +
                "contas importadas=" + contas.size() +
                '}';
    }
}
