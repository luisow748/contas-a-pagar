package com.luisow748.contas_a_pagar.domain.model;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Conta {
    private final Long id;
    private String nome;
    private String descricao;
    private final BigDecimal valor;
    private final String dataVencimento;
    private String dataPagamento;
    private String situacao;

    public Conta(String nome, String descricao, BigDecimal valor, String dataVencimento, String situacao) {
        this.id = null;
        this.nome = nome;
        this.descricao = descricao;
        this.valor = valor;
        this.dataVencimento = dataVencimento;
        this.situacao = situacao;
    }

    public Conta(Long id, String nome, String descricao, BigDecimal valor, String dataVencimento, String dataPagamento, String situacao) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
        this.valor = valor;
        this.dataVencimento = dataVencimento;
        this.dataPagamento = dataPagamento;
        this.situacao = situacao;
    }

    public Long getId() {
        return id;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public void setDataPagamento(String dataPagamento) {
        this.dataPagamento = dataPagamento;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public String getSituacao() {
        return situacao;
    }

    public String getDataVencimento() {
        return dataVencimento;
    }

    public String getDataPagamento() {
        return dataPagamento;
    }

    public void pagar(){
        this.situacao = "PAGO";
        setDataPagamento(LocalDate.now().format(java.time.format.DateTimeFormatter.ofPattern("dd-MM-yyyy")));
    }
}
