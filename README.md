# API Contas a Pagar

## Introdução

"Contas a Pagar" é uma API simples desenvolvida em Java 21 utilizando Spring e Postgres para gerenciar contas a pagar. Ele oferece funcionalidades para criar e alterar registros através de uma API Rest.

## Índice

- [Introdução](#introdução)
- [Instalação](#instalação)
- [Funcionalidades](#funcionalidades)
- [Tecnologias](#tecnologias)
- [Padrões](#padrões)
- [Estrutura](#estrutura)
- [Dependências](#dependências)
- [Configuração](#configuração)
- [Segurança e Autenticação](#segurança-e-autenticação)
- [Documentação](#documentação)
- [Exemplos](#exemplos)
- [Importação em lote](#importação-em-lote)
- [Contribuidores](#contribuidores)
- [Licença](#licença)

## Instalação

Para instalar o projeto, siga estes passos:

1. Clone o repositório:
   ```bash
   git clone https://gitlab.com/luisow748/contas-a-pagar.git
   ```
2. Navegue até o diretório do projeto:
   ```bash
   cd contas-a-pagar
   ```
3. Faça o build do projeto:
   ```bash
   ./gradlew bootJar
   ```   
4. Execute o projeto usando o Docker:
   ```bash
   # Exemplo com docker-compose
   docker-compose up -d --build
   ```

## Funcionalidades

- Incluir contas a pagar
- Alterar uma conta
- Importar arquivos com lotes de contas a pagar

## Tecnologias

- Java 21
- Spring Boot
- Postgres
- Docker
- Swagger
- JUnit
- Mockito
- Lombok
- Flyway

## Padrões

- O projeto utiliza conceitos de DDD - Domain Driven Design.
- A Importação / Extração em lote de contas foi implementada utilizando o padrão de projeto Strategy.
- O projeto utiliza o padrão de projeto Repository para acessar o banco de dados.
- O projeto utiliza o mecanismo de migração de banco de dados Flyway.

## Estrutura

A estrutura do projeto é organizada da seguinte forma:

```plaintext
.
├── src
│   ├── main
│   │   ├── java
│   │   │   └── com
│   │   │       └── luisow748
│   │   │           └── contas_a_pagar
│   │   │               ├── application
│   │   │               ├── domain
│   │   │               ├── infrastructure
│   │   │               └── interfaces


```


## Dependências

Liste as principais dependências necessárias para rodar o projeto:

- Java 21.
- Docker

## Configuração

### Segurança e Autenticação:

Por utilizar o Spring Security, é necessário configurar um usuário e senha para acessar o sistema. Para isso, siga os passos abaixo:
No arquivo application.yml, altere as propriedades para definir um usuário e senha para acessar o sistema. Exemplo:

```yaml
spring:
  security:
    user:
      name: "usuário"
      password: "senha"
```

## Documentação

- A documentação da API pode ser acessada em [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html).

## Exemplos

### Importação em lote:

- Para importar um arquivo com lotes de contas a pagar, siga os passos abaixo:
  1. Acesse a documentação do projeto em [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html).
  2. Clique na operação POST /api/contas-a-pagar/importar - "Importar Contas".
  3. Clique em "Try it out".
  4. Clique em "Choose File" e selecione o arquivo com os lotes de contas a pagar. Há um arquivo de exemplo em [src/main/resources/contas.csv](src/main/resources/contas.csv).

## Contribuidores

- Luís Fernando Porto - [luisow748](https://gitlab.com/luisow748)

## Licença

Este projeto está licenciado sob a [Licença MIT]().
